﻿using App.Core.Models;

namespace App.Core.Interfaces.Repositories
{
    public interface IBookRepository : IRepositoryBase<Book>
    {
        Book Get(int id);
    }
}
