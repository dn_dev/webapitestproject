﻿using System;
using App.Core.Enums;

namespace App.Core.Interfaces.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Gets the data context.
        /// </summary>
        IWebApiApplicationDbContext DbContext { get; }

        /// <summary>
        /// Gets a value indicating whether the opened transaction exists.
        /// </summary>
        bool TransactionOpened { get; }

        /// <summary>
        /// Saves all changes within unit of work.
        /// </summary>
        /// <returns>The number of objects written to the underlying database.</returns>
        /// <exception cref="System.ApplicationException">Validation Errors collection.</exception>
        /// <exception cref="System.Exception">Updating database error.</exception>
        int Save();

        /// <summary>
        /// Begins a transaction on the underlying store connection using the specified isolation level.
        /// </summary>
        /// <param name="transactionIsolationLevel">The transaction isolation level.</param>
        /// <exception cref="System.NotImplementedException">Multiple transactions are not implemented.</exception>
        void BeginTransaction(TransactionIsolationLevel transactionIsolationLevel = TransactionIsolationLevel.ReadCommitted);

        /// <summary>
        /// Commits the transaction.
        /// </summary>
        void CommitTransaction();

        /// <summary>
        /// Rollbacks the transaction.
        /// </summary>
        void RollbackTransaction();
    }
}
