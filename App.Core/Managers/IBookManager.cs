﻿using App.Core.Models;
namespace App.Core.Managers
{
    public interface IBookManager
    {
        Book[] GetAllBooks();
        Book GetBookById(int id);
        void AddBook(string bookName, string bookAuthor, int year);
        void EditBook(int bookId, string bookName, string bookAuthor, int year);
        void DeleteBook(int bookId);
    }
}
