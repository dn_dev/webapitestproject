﻿using System.Reflection;
using System.Web.Http;
using System.Web;
using Autofac;
using Autofac.Integration.WebApi;
using Autofac.Integration.Mvc;
using Microsoft.Owin;
using Owin;
using System.Web.Mvc;

[assembly: OwinStartup(typeof(WebApiApplication.Startup))]

namespace WebApiApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            var builder = new ContainerBuilder();
            // Register MVC controllers.
            builder.RegisterApiControllers(typeof(WebApiApplication).Assembly);
            Assembly dataAssembly = Assembly.Load("App.Data");
            Assembly businessAssembly = Assembly.Load("App.Business");

            builder.RegisterAssemblyTypes(dataAssembly)
                .Where(t => t.Name.EndsWith("UnitOfWork")).AsImplementedInterfaces().InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(dataAssembly)
                .Where(t => t.Name.EndsWith("Repository") || t.Name.EndsWith("DbContext"))
                .AsImplementedInterfaces().InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(businessAssembly)
                .Where(t => t.Name.EndsWith("Manager"))
                .AsImplementedInterfaces().InstancePerLifetimeScope();

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver =
                new AutofacWebApiDependencyResolver(container);
            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(GlobalConfiguration.Configuration);
            app.UseWebApi(GlobalConfiguration.Configuration);
        }
    }
}
