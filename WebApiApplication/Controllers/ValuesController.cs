﻿using System.Collections.Generic;
using System.Web.Http;
using App.Core.Managers;
using App.Core.Models;

namespace WebApiApplication.Controllers
{
    [AllowAnonymous]
    public class BookController : ApiController
    {
        private readonly IBookManager _bookManager;

        public BookController()
        {

        }
        public BookController(IBookManager bookManager)
        {
            this._bookManager = bookManager;
        }
        // GET api/values
        public IEnumerable<Book> GetBooks()
        {
            return _bookManager.GetAllBooks();
        }

        // GET api/values/5
        public Book GetBook(int id)
        {
            return _bookManager.GetBookById(id);
        }

        // POST api/values
        public void PostBook([FromBody]Book book)
        {
            if (ModelState.IsValid)
            {
                _bookManager.AddBook(book.Name, book.Author, book.Year);
            }
        }

        // PUT api/values/5
        public void PutBook(int id, [FromBody]Book book)
        {
            if (ModelState.IsValid)
            {
                _bookManager.EditBook(id, book.Name, book.Author, book.Year);
            }
        }

        // DELETE api/values/5
        public void DeleteBook(int id)
        {
            _bookManager.DeleteBook(id);
        }
    }
}
