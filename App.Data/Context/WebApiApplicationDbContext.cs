﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interfaces;
using App.Core.Models;
using App.Data.Mapping;

namespace App.Data.Context
{
    public class WebApiApplicationDbContext : DbContext, IWebApiApplicationDbContext
    {
        public WebApiApplicationDbContext() : base("name=WebApiApplicationEntities")
        {
            //this.Database.Log = txt => System.Diagnostics.Debug.WriteLine(txt);

            //The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
            //for the 'System.Data.SqlClient' ADO.NET provider could not be loaded.
            //Make sure the provider assembly is available to the running application.
            //See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.

            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public WebApiApplicationDbContext(string connectionStringName) : base("name=" + connectionStringName)
        {
            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        //public DbSet<Book> Books { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Database.SetInitializer(new WebApiApplicationDbContextInitializer());
            Database.SetInitializer<WebApiApplicationDbContext>(null);
            modelBuilder.Configurations.Add(new BookMap());
        }
    }
}
