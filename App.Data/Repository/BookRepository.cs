﻿using System.Linq;
using App.Core.Interfaces.Repositories;
using App.Core.Interfaces.UnitOfWork;
using App.Core.Models;

namespace App.Data.Repository
{
    public class BookRepository : RepositoryBase<Book>, IBookRepository
    {
        public BookRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Book Get(int id)
        {
            return this.Select(x => x.Id == id).FirstOrDefault();
        }
    }
}
