﻿using System.Linq;
using App.Core.Interfaces.Repositories;
using App.Core.Interfaces.UnitOfWork;
using App.Core.Managers;
using App.Core.Models;

namespace App.Business.Managers
{
    public class BookManager : IBookManager
    {
        private readonly IBookRepository _bookRepository;
        private readonly IUnitOfWork _unitOfWork;

        public BookManager(IUnitOfWork unitOfWork, IBookRepository bookRepository)
        {
            _unitOfWork = unitOfWork;
            _bookRepository = bookRepository;
        }

        public Book[] GetAllBooks()
        {
            return _bookRepository.SelectAll().ToArray();
        }

        public Book GetBookById(int id)
        {
            return _bookRepository.Get(id);
        }

        public void AddBook(string bookName, string bookAuthor, int bookYear)
        {
            var model = new Book
            {
                Name = bookName,
                Author = bookAuthor,
                Year = bookYear
            };
            _bookRepository.Insert(model);
            _unitOfWork.Save();
        }

        public void EditBook(int bookId, string bookName, string bookAuthor, int bookYear)
        {
            var record = _bookRepository.Get(bookId);
            if (record != null)
            {
                record.Name = bookName;
                record.Author = bookAuthor;
                record.Year = bookYear;
            }
            _unitOfWork.Save();
        }

        public void DeleteBook(int bookId)
        {
            var record = _bookRepository.Get(bookId);
            if (record != null)
            {
                _bookRepository.Delete(record);
            }
            _unitOfWork.Save();
        }
    }
}
